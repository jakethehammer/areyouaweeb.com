from django.shortcuts import render
from django.http import HttpResponse
from .forms import IDForm
from django.http import HttpResponseRedirect
from django.conf import settings
import steamapi
import urllib.request as urllib2
import re

def index(request):
    if request.method == 'POST':
        form = IDForm(request.POST)
        if form.is_valid():
            #process shit
            steamapi.core.APIConnection(api_key=settings.API_KEY, validate_key=True)
            gameCounter = 0
            weebCounter = 0
            try:
                user = steamapi.user.SteamUser(userurl=form.cleaned_data['id'])
                games = user.games
                for game in games:
                    page = (urllib2.urlopen("https://store.steampowered.com/app/" + str(game.appid))).read().decode('utf-8')
                    if "Anime" in page or "anime" in page or "Hentai" in page or "hentai" in page:
                        weebCounter += 1
                        print(game.name + ": Yes")
                    else:
                        print(game.name + ": :No")
                    gameCounter += 1
            except steamapi.errors.UserNotFoundError:
                return render(request, 'error.html', {'error': "That uwsew does not exist"})
            except steamapi.errors.AccessException:
                return render(request, 'error.html', {'error': "That uwsew does not have his pwofiwe set to puwbwic"})              
            return render(request, 'results.html', {'nGames': gameCounter, 'nWeebGames': weebCounter, 'weebPercentage': str((weebCounter / gameCounter) * 100) + "%"})
    else:
        form = IDForm()

    return render(request, 'index.html', {'form': form})
