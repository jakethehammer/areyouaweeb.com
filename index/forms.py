from django import forms

class IDForm(forms.Form):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('label_suffix', '') #override the colon at the end of form labels with nothing
        super(IDForm, self).__init__(*args, **kwargs)
    id = forms.CharField(label='https://steamcommunity.com/id/', max_length=20)
